# Cookiecutter Angular OIDC Template

This repository contains a [Cookiecutter](https://github.com/cookiecutter/cookiecutter) template for an Angular app.
This template includes all of the boilerplate you'd get from running `cookiecutter`,
allowing you to get started quickly and efficiently.

## Features

* a basic [Angular](https://angular.dev/overview) project
* with [OIDC](https://github.com/manfredsteyer/angular-oauth2-oidc) preconfigured
* e2e tests using [Playwright](https://playwright.dev/docs/intro)
* unit tests using [Vitest](https://vitest.dev/guide/)

## Precondition

Install cookiecutter following the [instructions here](https://github.com/cookiecutter/cookiecutter)

## Usage
 
Run the following command from the directory where you want the project folder containing the Angular project to be created:

```bash
cookiecutter git+ssh://git@gitlab.com/nxt/public/angular-oidc-template.git 
```

This will prompt you for some information (such as the project name, author name, and so on), 
and then generate a new project in a directory with the same name as your project.

## Linting

Although `editorconfig`, `linters` and `formatters` are configured, 
please be sure to also install and apply our [styleguide template](https://gitlab.com/nxt/public/styleguide-template)
after initializing the `cookiecutter` template.
