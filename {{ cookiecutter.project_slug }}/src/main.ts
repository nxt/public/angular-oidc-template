import { bootstrapApplication } from '@angular/platform-browser';
import { appConfig } from './app/app.config';
import { AppComponent } from './app/app.component';
import {enableProdMode} from "@angular/core";
import {environment} from "./environments/environment";
import {registerLocaleData} from "@angular/common";
import localeDe from '@angular/common/locales/de-CH';

if (environment.production) {
  enableProdMode();
}

function bootstrap() {
  registerLocaleData(localeDe, 'de-CH');
  bootstrapApplication(AppComponent, appConfig)
    .catch((err) => console.error(err));
}

if (document.readyState === 'complete') {
  bootstrap()
} else {
  document.addEventListener('DOMContentLoaded', bootstrap);
}
