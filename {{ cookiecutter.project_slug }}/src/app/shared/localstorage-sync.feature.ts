import { getState, patchState, signalStoreFeature, withHooks } from '@ngrx/signals';
import { effect } from '@angular/core';
// eslint-disable-next-line import/no-unresolved
import { StateSignal } from '@ngrx/signals/src/state-signal';

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export function withLocalStorageSync(storageKey: string, keysToInclude: string[]) {
  return signalStoreFeature(
    withHooks({
      onInit(store) {
        localStorageSync(store, storageKey, keysToInclude);
      },
    })
  );
}

export function localStorageSync<State extends object>(
  store: StateSignal<State>,
  storageKey: string,
  keysToInclude: string[]
): void {
  const oldState = localStorage.getItem(storageKey);
  if (oldState?.length) {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
    patchState(store, JSON.parse(oldState));
  }
  effect(() => {
    const state = getState(store);
    Object.keys(state)
      .filter((k) => !keysToInclude.includes(k))
      .forEach((key) => {
        // eslint-disable-next-line
        // @ts-ignore
        delete state[key];
      });
    localStorage.setItem(storageKey, JSON.stringify(state));
  });
}
