import { TestBed } from '@angular/core/testing';
import {AppComponent} from "./app.component";
import { describe, beforeEach, it, expect } from 'vitest'

describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppComponent],
    }).compileComponents();
  });

  it('should add plus one', () => {
    const fixture = TestBed.createComponent(AppComponent);

    expect(fixture.componentInstance.addPlusOne(1)).toEqual(2);
  });
});
