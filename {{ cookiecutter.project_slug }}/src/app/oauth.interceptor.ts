import { inject, Injectable } from '@angular/core';
import { OAuthResourceServerErrorHandler, OAuthService } from 'angular-oauth2-oidc';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable, from, switchMap } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class DefaultOAuthInterceptor implements HttpInterceptor {
  private readonly authService = inject(OAuthService);
  private readonly errorHandler = inject(OAuthResourceServerErrorHandler);

  private checkUrl(url: string): boolean {
    const allowedUrls = ['/api'];
    const found = allowedUrls.find((u) => url.startsWith(u));
    return !!found;
  }

  public intercept(req: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const url = req.url.toLowerCase();

    if (!this.checkUrl(url)) {
      return next.handle(req);
    }

    const expiration = this.authService.getAccessTokenExpiration();
    const buffer = 1000 * 60; // 1 minute
    if (expiration <= 0 || expiration - buffer < Date.now()) {
      return from(this.authService.refreshToken()).pipe(switchMap(() => this.addBearerToken(req, next)));
    }

    return this.addBearerToken(req, next);
  }

  private addBearerToken(req: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const token = this.authService.getAccessToken();
    const header = `Bearer ${token}`;

    const headers = req.headers.set('Authorization', header);

    req = req.clone({ headers });

    return next
      .handle(req)
      .pipe(catchError((err: HttpResponse<unknown>) => this.errorHandler.handleError(err))) as Observable<
      HttpEvent<unknown>
    >;
  }
}
