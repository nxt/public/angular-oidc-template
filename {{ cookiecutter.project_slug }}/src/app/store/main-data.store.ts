import {patchState, signalStore, withHooks, withMethods, withState} from "@ngrx/signals";
import {withLocalStorageSync} from "../shared/localstorage-sync.feature";

export type User = {
  id: string,
  username: string,
  name: string,
  email: string,
}

type MainDataState = {
  loggedInUser: User
}

const initialState: MainDataState = {
  loggedInUser: {
    id: '',
    username: '',
    name: '',
    email: '',
  }
}

export const MainDataStore = signalStore(
  { providedIn: 'root' },
  withState(initialState),
  withMethods((store) => ({
    setLoggedInUser: (loggedInUser: User) => patchState(store, {loggedInUser})
  })),
  withLocalStorageSync('main-data', Object.keys(initialState)),
);

export type MainDataStore = InstanceType<typeof MainDataStore>;
