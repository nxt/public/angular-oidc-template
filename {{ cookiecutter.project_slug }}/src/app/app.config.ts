import {APP_INITIALIZER, ApplicationConfig, ErrorHandler, importProvidersFrom} from '@angular/core';
import {provideRouter, Router} from '@angular/router';

import { routes } from './app.routes';
import {environment} from "../environments/environment";
import {ServiceWorkerModule} from "@angular/service-worker";
import {provideAnimations} from "@angular/platform-browser/animations";
import {HTTP_INTERCEPTORS, provideHttpClient, withFetch, withInterceptorsFromDi} from "@angular/common/http";
import {DefaultOAuthInterceptor} from "./oauth.interceptor";
import {AuthConfig, OAuthService, provideOAuthClient} from "angular-oauth2-oidc";
import * as Sentry from '@sentry/angular-ivy';
import {AppConfigService} from "../app-config.service";
import {configureSentry} from "../sentry.config";
import {MainDataStore} from "./store/main-data.store";

export const appConfig: ApplicationConfig = {
  providers: [
    provideRouter(routes),
    importProvidersFrom(
      ServiceWorkerModule.register('ngsw-worker.js', {
        enabled: environment.production,
        registrationStrategy: 'registerImmediately',
      })
    ),
    {
      provide: HTTP_INTERCEPTORS,
      useClass: DefaultOAuthInterceptor,
      multi: true,
    },
    provideHttpClient(withFetch(), withInterceptorsFromDi()),
    provideAnimations(),
    {
      provide: APP_INITIALIZER,
      useFactory: initializeAppFactory,
      deps: [
        OAuthService,
        AppConfigService,
        MainDataStore,
      ],
      multi: true,
    },
    ...(environment.production
      ? [
        {
          provide: ErrorHandler,
          useValue: Sentry.createErrorHandler({
            showDialog: true,
          }),
        },
        {
          provide: Sentry.TraceService,
          deps: [Router],
        },
      ]
      : []),
    provideOAuthClient(),
  ]
};

function initializeAppFactory(
  oauthService: OAuthService,
  configService: AppConfigService,
  mainDataStore: MainDataStore,
): () => Promise<unknown> {
  return async () => {
    const appConfig = await configService.reload(environment);

    if (appConfig.auth === undefined) {
      return Promise.reject('The server did not provide a valid Auth config.');
    }

    const authCodeFlowConfig: AuthConfig = {
      issuer: appConfig.auth.issuerUrl,
      redirectUri: appConfig.baseUrl,
      clientId: appConfig.auth.clientId,
      requireHttps: appConfig.auth.requireTls,
      responseType: 'code',
      scope: 'openid profile email offline_access',
      postLogoutRedirectUri: appConfig.baseUrl,
      timeoutFactor: 0.75,
      sessionChecksEnabled: false,
      clockSkewInSec: 0,
    };

    oauthService.configure(authCodeFlowConfig);
    oauthService.setupAutomaticSilentRefresh();
    oauthService.setStorage(window.localStorage);

    await oauthService.loadDiscoveryDocument();

    if (oauthService.getRefreshToken() !== null) {
      try {
        if (!oauthService.hasValidIdToken() || !oauthService.hasValidAccessToken()) {
          await oauthService.refreshToken();
        }
      } catch (e) {
        // eslint-disable-next-line no-console
        console.log('Can not refresh token');
        window.localStorage.clear();
        await oauthService.tryLogin();
      }
    } else {
      await oauthService.tryLogin();
    }

    if (!oauthService.hasValidIdToken() || !oauthService.hasValidAccessToken()) {
      if (!environment.production) {
        window.localStorage.clear();
      }
      oauthService.initLoginFlow();
      return new Promise(() => {});
    }

    if (oauthService.hasValidIdToken() && oauthService.hasValidAccessToken()) {
      const identityClaims = oauthService.getIdentityClaims();
      oauthService.events.subscribe((e) => {
        if (e.type === 'token_refresh_error') {
          window.localStorage.clear();
          location.reload();
        }
      });

      const user = {
        id: identityClaims['sub'] as string,
        username: identityClaims['preferred_username'] as string,
        name: identityClaims['name'] as string,
        email: identityClaims['email'] as string,
      };

      Sentry.setUser(user);
      mainDataStore.setLoggedInUser(user);
      configureSentry(configService);
      return;
    }
    return;
  };
}
