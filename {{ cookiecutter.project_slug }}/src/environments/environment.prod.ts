import {Environment} from "./environment.type";
import {ErrorHandler} from "@angular/core";
import * as Sentry from '@sentry/angular-ivy';
import {Router} from "@angular/router";

export const environment: Environment = {
  production: true,
  configEndpoint: '/config.json',
  envSpecificModules: [
    {
      provide: ErrorHandler,
      useValue: Sentry.createErrorHandler({
        showDialog: false,
      }),
    },
    {
      provide: Sentry.TraceService,
      deps: [Router],
    },
  ],
};
