import { EnvironmentProviders, Provider } from '@angular/core';

export interface Environment {
  production: boolean;
  configEndpoint: string;
  envSpecificModules: EnvironmentProviders[] | Provider[];
}
