import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Environment} from "./environments/environment.type";

export interface Configuration {
  environment: 'local' | 'dev' | 'prod';
  baseUrl: string;
  auth?: {
    issuerUrl: string;
    requireTls: boolean;
    clientId: string;
  };
  webapi: {
    http: string;
  },
  applicationInsights?: {
    connectionString: string;
    correlationHeaderDomains: string[] | undefined;
  };
  sentryDsn: string;
}

@Injectable({
  providedIn: 'root',
})
export class AppConfigService {
  constructor(private http: HttpClient) {}

  public available = false;
  public configuration: Configuration | null = null;

  public reload(env: Environment): Promise<Configuration> {
    return new Promise((resolve, reject) => {
      this.http.get<Configuration>(env.configEndpoint).subscribe({
        next: (data) => {
          this.available = true;
          this.configuration = data;
          resolve(data);
        },
        error: (err) => {
          // eslint-disable-next-line no-console
          console.error(`Failed to fetch the configuration from '${env.configEndpoint}':`, err);
          reject(err);
        },
      });
    });
  }
}
