import { Page } from '@playwright/test';
import { BasePage } from '../pages/base-page';
import type { Configuration } from '../../src/app-config.service';

const getLocalStorageAdmin = (): object => {
  const date = new Date();
  date.setMinutes(date.getMinutes() + 15);
  const time = String(date.getTime());
  return {
    id_token:
      'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJiM3FXWTJ3eFo2R1FSRkxIYTlTdjczUWZnbmJuU01hLTlvbV9ZSlFUNENJIn0.eyJleHAiOjE2NjY3NzIzODgsImlhdCI6MTY2Njc3MjA4OCwiYXV0aF90aW1lIjoxNjY2NzcwMjA3LCJqdGkiOiIxMzY3ZDAyYy04MTY5LTRkNTgtYjQ0MS1lOWNlYTlhMWYyNWYiLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvcmVhbG1zL254dCIsImF1ZCI6InRhY3MiLCJzdWIiOiI1OTVlZWU4YS00YjRjLTQxMzUtYjE0Yy1hZmIyNjVmZDQ1MmEiLCJ0eXAiOiJJRCIsImF6cCI6InRhY3MiLCJub25jZSI6ImRITldPR1V0TmpWQ1JGRjFNVE5NYUhSSVJVdEdZa1ppWlg1WVNETjFZMlZHUlM1UmRIUTRSbWQ0ZVc1diIsInNlc3Npb25fc3RhdGUiOiJmYzY4ZGViOC1jOThlLTQ3NjItYTI1Yi04NzEyNDkwMDk2NjAiLCJhdF9oYXNoIjoiaTJQenkxZ0FlLTlhT1hGNUgwTUxUQSIsInNpZCI6ImZjNjhkZWI4LWM5OGUtNDc2Mi1hMjViLTg3MTI0OTAwOTY2MCIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJlbXBsb3llZV9pZCI6ImFiZTFkMTM4LWM4YWUtNDhkNC04MTI3LWM3NjY3OGMyZDFlYSIsIm5hbWUiOiJLdXJ0IE1laWVyIiwicHJlZmVycmVkX3VzZXJuYW1lIjoibnh0IiwiZ2l2ZW5fbmFtZSI6Ikt1cnQiLCJmYW1pbHlfbmFtZSI6Ik1laWVyIn0.hdxR2QAcaNktGlaDz3DYsm90pXPyXT2__hMCQo0T-HVpBEGWhvi20zZbQxgAYi5pylUcoqkGR_4vMDsVBF-VbOlbvevDHsGdDPy2Te3nS4nP3ZYs6BOZHdvGaO7IaPugOXAPQyg4zqJiigNn1RYGKUFEOxB6cbsqbdgpnYwsLq2l0fr2sJwfS25X9ZkLu3rEsJ9fiCkMrp3g7KwrowbSaTlb4mjXvP-S3H-WcJg4xqL4Qi8z0vKUXnCnAOiyoskRAXpbd1RIfTYDQKI8ljDWtev1L8jR3VwXK_09ul1bVY24Ic1Le7f4laFr_RmjcYT57cLTFPxz_MUtqgwszacNxg',
    access_token:
      'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJiM3FXWTJ3eFo2R1FSRkxIYTlTdjczUWZnbmJuU01hLTlvbV9ZSlFUNENJIn0.eyJleHAiOjE2NjY3NzIzODgsImlhdCI6MTY2Njc3MjA4OCwiYXV0aF90aW1lIjoxNjY2NzcwMjA3LCJqdGkiOiJjODIxMjY2Yy1kZmUwLTQyOWUtOWNiZS05NTJiYjMzNTU5NTciLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvcmVhbG1zL254dCIsImF1ZCI6ImFjY291bnQiLCJzdWIiOiI1OTVlZWU4YS00YjRjLTQxMzUtYjE0Yy1hZmIyNjVmZDQ1MmEiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJ0YWNzIiwibm9uY2UiOiJkSE5XT0dVdE5qVkNSRkYxTVROTWFIUklSVXRHWWtaaVpYNVlTRE4xWTJWR1JTNVJkSFE0Um1kNGVXNXYiLCJzZXNzaW9uX3N0YXRlIjoiZmM2OGRlYjgtYzk4ZS00NzYyLWEyNWItODcxMjQ5MDA5NjYwIiwiYWxsb3dlZC1vcmlnaW5zIjpbIioiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbImRlZmF1bHQtcm9sZXMtbnh0Iiwib2ZmbGluZV9hY2Nlc3MiLCJhZG1pbiIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJvcGVuaWQgcHJvZmlsZSB0YWNzIG9mZmxpbmVfYWNjZXNzIGVtYWlsIiwic2lkIjoiZmM2OGRlYjgtYzk4ZS00NzYyLWEyNWItODcxMjQ5MDA5NjYwIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImVtcGxveWVlX2lkIjoiYWJlMWQxMzgtYzhhZS00OGQ0LTgxMjctYzc2Njc4YzJkMWVhIiwibmFtZSI6Ikt1cnQgTWVpZXIiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJueHQiLCJnaXZlbl9uYW1lIjoiS3VydCIsImZhbWlseV9uYW1lIjoiTWVpZXIifQ.kZd2FobinXF3BR-EhWAOEdc6EJ-Jb5LeJybNYi0FozyBFeAF4eeOu8YvKBSz9bsgruMYqPwzp4Y833r6nJIN47UQ32ME91qE8oDh8ZQqXk9hVjJ19n2VI7ZBzowIQhrUCTf129FuL2CRtQQ06fv0dUSqPbsN1Oc-LJdYsgVcDNZLUp4sLBYYhihejLa4Q37uWcQ0y3Fz5fFfUUD1a0A1zWUybflYzNKe8rcj1Ff5T_WfyytXoZ5xetNCd6o9KPjtAnhPdaK0nOL4hhYhmrzvDhOUPZIj8ZNgDUj3E5xglldrm5dBvGYnWdNG_cE7kAPzz8nIU10ln178P8RlkOM9nA',
    id_token_claims_obj: {
      exp: 1666771439,
      iat: 1666771139,
      auth_time: 1666770207,
      jti: '7d2d64ce-d96c-4451-8cc9-eb1f95560d60',
      iss: '{{ cookiecutter.dev_oauth_issuer_url }}',
      aud: '{{ cookiecutter.project_slug }}',
      sub: '595eee8a-4b4c-4135-b14c-afb265fd452a',
      typ: 'ID',
      azp: '{{ cookiecutter.project_slug }}',
      nonce: 'dHNWOGUtNjVCRFF1MTNMaHRIRUtGYkZiZX5YSDN1Y2VGRS5RdHQ4Rmd4eW5v',
      session_state: 'fc68deb8-c98e-4762-a25b-871249009660',
      at_hash: 'imvKFLQoxkhcfbn0sz7lYA',
      sid: 'fc68deb8-c98e-4762-a25b-871249009660',
      email_verified: true,
      employee_id: 'c865bf82-2c26-4ddb-80f7-0dd7217725f3',
      name: 'Max Mustermann',
      preferred_username: 'mm',
      given_name: 'Max',
      family_name: 'Mustermann',
      roles: ['admin'],
    },
    id_token_expires_at: time,
    access_token_expires_at: time,
    expires_at: time,
    access_token_stored_at: time,
    id_token_stored_at: time,
  };
};

const getLocalStorageUser = (): object => {
  const date = new Date();
  date.setMinutes(date.getMinutes() + 15);
  const time = String(date.getTime());
  return {
    id_token:
      'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJiM3FXWTJ3eFo2R1FSRkxIYTlTdjczUWZnbmJuU01hLTlvbV9ZSlFUNENJIn0.eyJleHAiOjE2Njc0NjY4MjgsImlhdCI6MTY2NzQ2NjUyOCwiYXV0aF90aW1lIjoxNjY3NDY2NTI3LCJqdGkiOiI1NTNkNTM4My1mYTc3LTQwNjUtYjAzYi1jMTcyMWI2ZDE3ODkiLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvcmVhbG1zL254dCIsImF1ZCI6InRhY3MiLCJzdWIiOiJjNWIyYjYzZC1kNjE1LTQzMzMtOGFmYi1kNzY2OTFkZTU2M2UiLCJ0eXAiOiJJRCIsImF6cCI6InRhY3MiLCJub25jZSI6IlF6ZGxZak5CVDAwMlgycEZOV3AtUmtGV056SXViMDlCVUVGaE9EbHJjbkJhTm1WSE5ERmtablp1VGxaVyIsInNlc3Npb25fc3RhdGUiOiI1N2ZlYzdiMy1hZGYxLTQyMjktODg3Zi02NDE4MGJmMzc2MjMiLCJhdF9oYXNoIjoiWElvTUFGTEUwWkp3dE9MT3dFNDVQUSIsInNpZCI6IjU3ZmVjN2IzLWFkZjEtNDIyOS04ODdmLTY0MTgwYmYzNzYyMyIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJlbXBsb3llZV9pZCI6ImE3ODA5ZjhmLTg4NmUtNDA5NC1hMmZjLWY4ODA1OWNlMTA3YyIsIm5hbWUiOiJCcnlhbm5hIEhpdHpiZXJnZXIiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJiaGkiLCJnaXZlbl9uYW1lIjoiQnJ5YW5uYSIsImZhbWlseV9uYW1lIjoiSGl0emJlcmdlciJ9.RhMrn1b7qYwYzhJfE-_yzDpa7-X30aR7-fp8h_RMS120y4G-huieXCXHCOWUvtGZ1Y9BnlnwLys0o7RwEjxvK_JEaX9CUG_zWOwfMe7ckUWtm6Bq12MSecwgExPaeeJ0VO8DGNCeckQ2qqvyXMCGhgzsk7gXx9Qc-0b8UEQqh1MD1LXMGdSuGaiJ9Ki3sDciKY23sttC_rdVGqfSuFsFJbNgY0AFbcqamdKWHPEu8576NcRIdvWvAR8QCkRNz0qrGOem6QR1jZpZnSAmLx91m4iHz91eWXprfrbEgXKn1D-GPf84mwpgY60-sb56xZ9SeHPF9T8ozIAvEEVDT-kqNQ',
    access_token:
      'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJiM3FXWTJ3eFo2R1FSRkxIYTlTdjczUWZnbmJuU01hLTlvbV9ZSlFUNENJIn0.eyJleHAiOjE2Njc0NjcxMzIsImlhdCI6MTY2NzQ2NjgzMiwiYXV0aF90aW1lIjoxNjY3NDY2NTI3LCJqdGkiOiJlMDc4NzhiYy1kY2I5LTRmNjctOTM1Ni0xMTk3ODI0MmMzYmYiLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvcmVhbG1zL254dCIsImF1ZCI6ImFjY291bnQiLCJzdWIiOiJjNWIyYjYzZC1kNjE1LTQzMzMtOGFmYi1kNzY2OTFkZTU2M2UiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJ0YWNzIiwibm9uY2UiOiJRemRsWWpOQlQwMDJYMnBGTldwLVJrRldOekl1YjA5QlVFRmhPRGxyY25CYU5tVkhOREZrWm5adVRsWlciLCJzZXNzaW9uX3N0YXRlIjoiNTdmZWM3YjMtYWRmMS00MjI5LTg4N2YtNjQxODBiZjM3NjIzIiwiYWxsb3dlZC1vcmlnaW5zIjpbIioiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbImRlZmF1bHQtcm9sZXMtbnh0Iiwib2ZmbGluZV9hY2Nlc3MiLCJ1bWFfYXV0aG9yaXphdGlvbiJdfSwicmVzb3VyY2VfYWNjZXNzIjp7ImFjY291bnQiOnsicm9sZXMiOlsibWFuYWdlLWFjY291bnQiLCJtYW5hZ2UtYWNjb3VudC1saW5rcyIsInZpZXctcHJvZmlsZSJdfX0sInNjb3BlIjoib3BlbmlkIHByb2ZpbGUgdGFjcyBvZmZsaW5lX2FjY2VzcyBlbWFpbCIsInNpZCI6IjU3ZmVjN2IzLWFkZjEtNDIyOS04ODdmLTY0MTgwYmYzNzYyMyIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJlbXBsb3llZV9pZCI6ImE3ODA5ZjhmLTg4NmUtNDA5NC1hMmZjLWY4ODA1OWNlMTA3YyIsIm5hbWUiOiJCcnlhbm5hIEhpdHpiZXJnZXIiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJiaGkiLCJnaXZlbl9uYW1lIjoiQnJ5YW5uYSIsImZhbWlseV9uYW1lIjoiSGl0emJlcmdlciJ9.tGW2Br5akYTcopFIUv8bg2t0zibZem5BjubZ_bLHDC8laepZr2Ow59-0FKmPUvm9FAFnmf2MOIOktvmvcYcza8cq7SYJOq0WLVU9fEAk_s17kXn7WiwF0ijOgUel_mQBqVAj6SMQ7pGE-iKMCu7vm6dciuXclRtQmZj-g-gXxpXyDaxz91JcsUnFoN6F9b7nPExfYwSfvi0jmtUB0AHndpJBykXYtOgJnMrc_TFrmlWmR_NVu8obrXRmliUpIq2WubmIh9hYwEpQIVzjv_lZ63hsmU9BKMKpOQbnPofiIYexO8_TNJMmtpEAO30z-nLF8SPQpHJgCfiKnJ9GpdIIDw',
    id_token_claims_obj: {
      exp: 1667466828,
      iat: 1667466528,
      auth_time: 1667466527,
      jti: '553d5383-fa77-4065-b03b-c1721b6d1789',
      iss: '{{ cookiecutter.dev_oauth_issuer_url }}',
      aud: '{{ cookiecutter.project_slug }}',
      sub: 'c5b2b63d-d615-4333-8afb-d76691de563e',
      typ: 'ID',
      azp: '{{ cookiecutter.project_slug }}',
      nonce: 'QzdlYjNBT002X2pFNWp-RkFWNzIub09BUEFhODlrcnBaNmVHNDFkZnZuTlZW',
      session_state: '57fec7b3-adf1-4229-887f-64180bf37623',
      at_hash: 'XIoMAFLE0ZJwtOLOwE45PQ',
      sid: '57fec7b3-adf1-4229-887f-64180bf37623',
      email_verified: true,
      employee_id: 'c865bf82-2c26-4ddb-80f7-0dd7217725f3',
      name: 'Max Mustermann',
      preferred_username: 'mm',
      given_name: 'Max',
      family_name: 'Mustermann',
      roles: [],
    },
    id_token_expires_at: time,
    access_token_expires_at: time,
    expires_at: time,
    access_token_stored_at: time,
    id_token_stored_at: time,
  };
};

export const setup = async (
  page: Page,
  admin: boolean,
  fixture: BasePage,
): Promise<void> => {

  await page.context().addInitScript(
    (storageValues) => {
      Object.entries(storageValues).forEach(([key, value]) => {
        window.localStorage.setItem(key, typeof value === 'string' ? value : JSON.stringify(value));
      });
    },
    admin ? getLocalStorageAdmin() : getLocalStorageUser()
  );

  await page.route('{{ cookiecutter.dev_oauth_issuer_url }}/.well-known/openid-configuration', (route) =>
    route.fulfill({
      status: 200,
      path: 'e2e-tests/fixtures/well-known.json',
    })
  );

  await page.route('{{ cookiecutter.dev_oauth_issuer_url }}/protocol/openid-connect/certs', (route) =>
    route.fulfill({
      status: 200,
      path: 'e2e-tests/fixtures/certs.json',
    })
  );

  await page.route('**/config', (route) =>
    route.fulfill({
      status: 200,
      body: JSON.stringify({
        environment: 'local',
        applicationInsights: {
          connectionString: '',
          correlationHeaderDomains: [],
        },
        baseUrl: 'http://localhost:4200',
        auth: {
          clientId: '{{ cookiecutter.dev_oauth_client_id }}',
          issuerUrl: '{{ cookiecutter.dev_oauth_issuer_url }}',
          requireTls: false,
        },
        webapi: {
          http: '{{ cookiecutter.e2e_api_endpoint }}'
        },
        sentryDsn: ''
      } satisfies Configuration),
    })
  );
  await page.goto(fixture.startUrl);
};
