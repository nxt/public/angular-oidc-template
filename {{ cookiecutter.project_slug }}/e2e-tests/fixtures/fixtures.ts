import { Page, test as base } from '@playwright/test';
import { setup } from './commands';
import {ExamplePage} from "../pages/example-page";

type FixtureFactories = {
  createExamplePage: (exampleData: string[]) => Promise<ExamplePage>;
};

export const test = base.extend<FixtureFactories>({
  createExamplePage: async ({ page }, use) => {
    await use(async (exampleData: string[] = []) => {
      const examplePage = new ExamplePage(page, exampleData);
      await setup(page, true, examplePage);
      return examplePage;
    });
    await clearStorage(page);
  },
});

const clearStorage = async (page: Page): Promise<void> => {
  await page.evaluate(() => {
    window.localStorage.clear();
    window.sessionStorage.clear();
  });
};
