import { expect, Page } from '@playwright/test';

export class BasePage {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  data: { [key: string]: any } = {};

  constructor(
    public page: Page,
    public startUrl: string
  ) {}

  protected pressButton(name: string): () => Promise<void> {
    return async () => await this.page.getByRole('button', { name: name, exact: true }).click();
  }

  protected pressRadioButton(name: string): () => Promise<void> {
    return async () => await this.page.getByRole('radio', { name: name, exact: true }).click();
  }

  protected setInput(label: string): (value: string) => Promise<void> {
    return async (value: string): Promise<void> => {
      await this.page.getByLabel(label).click();
      await this.page.getByLabel(label).fill(value);
    };
  }

  protected setCalendar(label: string): (value: string) => Promise<void> {
    return async (value: string): Promise<void> => {
      await this.page.getByLabel(label).click();
      await this.page.keyboard.press('Escape');
      await this.page.getByLabel(label).clear();
      await this.page.getByLabel(label).type(value);
    };
  }

  protected setDropdown(testId: string): (value: string) => Promise<void> {
    return async (value: string, exact = true): Promise<void> => {
      await this.page.getByTestId(testId).getByRole('button', { name: 'dropdown trigger' }).click();
      await this.page.getByRole('option', { name: value, exact: true }).click();
      await this.page.waitForTimeout(250); // wait for dropdown to close
    };
  }
}
