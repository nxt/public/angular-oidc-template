import { Locator, Page } from '@playwright/test';
import { BasePage } from './base-page';

export class ExamplePage extends BasePage {
  readonly testButton: Locator;

  constructor(page: Page, exampleData: string[]) {
    super(page, '/');
    this.data['exampleData'] = exampleData;

    this.testButton = this.page.getByRole('button', { name: 'Test' });
  }

  async clickButton(): Promise<void> {
    await this.testButton.click();
  }
}
