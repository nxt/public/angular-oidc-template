import { test } from '../fixtures/fixtures';
import {exampleData} from "../fixtures/data";
import { expect } from '@playwright/test';

test.describe('An example test', () => {
  test('click the button on the start page', async ({ createExamplePage }) => {
    const examplePage = await createExamplePage(exampleData);

    await examplePage.clickButton();

    await expect(examplePage.page.getByText('Simple E2E Test Result')).toBeVisible();
  })
})
